const PORT = 3000

const express = require("express")

const app = express();

const bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// get data from query url
app.get("/todos/search", (req, res) => {
    res.send(req.query)
})

// get data from params
app.get("/todos/:id", (req, res) => {
    res.send(req.params.id)
})

// get data from body
app.post("/todos", (req, res) => {
    res.send(req.body)
})


app.get("/", (req, res) => {
    res.send({
        version: "1.0.0",
        api_name: "exdpress.js demo"
    })
})



app.listen(PORT, () => console.log(`application runnning on port ${PORT}`))